var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// code untuk memanggil function readBooks
var jumlahBuku = books.length; 
var time = 10000; 

function execute(time, ind, books) { 
    readBooksPromise(time, books[ind]) 
    .then(function (remainingTime) { 
        time = remainingTime; 
        books = books - 1; 
        if (books > 0) { 
            execute(time, ind+1, books); 
        } 
    }) 
    .catch(function (error) { 

    }) 
} 

execute(time, 0, books);